

#mix release.clean --implode --no-confirm
export MIX_ENV=prod
mix compile && cd apps/universe && mix compile && mix release --verbosity=verbose &&
cd ../.. &&  cd apps/universe_web && brunch build && mix compile && mix phoenix.digest && mix release --verbosity=verbose
