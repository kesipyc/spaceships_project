

#mix release.clean --implode --no-confirm
export MIX_ENV=prod
mix compile && mix release.clean && cd apps/universe_web && brunch build && mix compile && mix phoenix.digest && cd ../.. && cd apps/universe && mix compile && cd ../.. && mix release --verbosity=verbose
