defmodule UniverseWeb.PageController do
  use UniverseWeb.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
