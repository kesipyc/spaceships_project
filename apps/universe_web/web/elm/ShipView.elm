module ShipView where


import StartApp
import Effects exposing (Effects, Never)
import Task exposing (Task)

import Signal exposing(Address)
import Time

import Color exposing (..)
import Graphics.Element exposing (..)
import Graphics.Collage exposing (..)
import Window
import Html

-- UTILS
minTuple : (Int, Int) -> (Int, Int)
minTuple (a, b) =
  if a < b then (a, a) else (b, b)

mulTuple : Float -> (Int, Int) -> (Int, Int)
mulTuple x (a,b) =
  (round (x * toFloat a), round (x * toFloat b))

--minSignalTuple : Signal(Int, Int) -> Signal(Int, Int)
--minSignalTuple s =
--  Signal.map minTuple s

scale : (Int, Int) -> (Int, Int) -> (Float, Float)
scale (x, y) (maxX, maxY) =
  (2.0 * (toFloat x / toFloat maxX) - 1.0, 2.0 * (toFloat y / toFloat maxY) - 1.0)

scaleForView : (Float, Float) -> (Float, Float) -> (Float, Float)
scaleForView (x, y) (scaleX, scaleY) =
               (x * scaleX, y * scaleY)


-- MODEL

type alias Vector =
  (Int, Int)

type alias Ship =
  { position : Vector
  , speed : Vector
  }

type alias Model =
  { ships : List Ship
  , viewSize : (Int, Int)
  }
                 

worldSize: (Int, Int)
worldSize = (1000,1000)
                         
init : (Model, Effects Action)
init =
    ( { ships = []
      , viewSize = (1000, 1000)
      }
    , Effects.none)


-- UPDATE

type Action = NoOp
            | SetShips (List Ship)
            | ViewResized (Int, Int)

update : Action -> Model -> (Model, Effects Action)
update action model =
  case action of
    NoOp -> (model, Effects.none)
    SetShips ships -> ( { model | ships = ships }, Effects.none)
    ViewResized (w, h) -> ( { model | viewSize = (w, h) }, Effects.none)
                     


-- VIEW

view: Address Action -> Model -> Html.Html
view address model =
--  Html.text (toString(w) ++ " " ++ toString(h)
    let
      (w, h) = model.viewSize
      (fitX, fitY) = mulTuple 0.9 (w, h)
    in
      Html.fromElement
          (container fitX fitY midTop (viewUniverse (minTuple (fitX, fitY)) model))


viewUniverse: (Int, Int) -> Model -> Element
viewUniverse (w, h) model =
  collage w h (List.append
                 [
                  filled black (rect (toFloat w) (toFloat h) )
                 ]
                 (viewShips (w, h) model.ships)
              )

viewShips: (Int, Int) -> List Ship -> List Form
viewShips (w,h) ships =
  List.map (viewShip (w,h)) ships

viewShip: (Int, Int) -> Ship -> Form
viewShip (w,h) ship =
  move
    (scaleForView
       (scale ship.position worldSize)
       ((toFloat w/2), (toFloat h/2))
    )
    (filled white (circle 2))


-- SIGNALS
app : StartApp.App Model
app =
  StartApp.start
    { init = init
    , update = update
    , view = view
    , inputs = [windowResized, incomingActions]
    }

main : Signal Html.Html
main =
  app.html

incomingActions : Signal Action
incomingActions =
  Signal.map SetShips shipsList

windowResized : Signal Action
windowResized =
  Signal.map ViewResized Window.dimensions

refreshRequestSignal : Signal Time.Time
refreshRequestSignal = Time.fps 30

-- PORTS
     
port tasks : Signal (Task Never ())
port tasks =
  app.tasks

port shipsList : Signal (List Ship)

port refreshRequest : Signal Time.Time
port refreshRequest =
  refreshRequestSignal
--  Signal.map2 view Window.dimensions (Signal.constant(init))

