defmodule UniverseWeb.ShipChannel do
  use UniverseWeb.Web, :channel

  def join("ships:all", payload, socket) do
    if authorized?(payload) do
      send self(), :after_join
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_info(:after_join, socket) do
#    ships =
#      [
#        %{ position: [100, 900], speed: [5, 5] },
#        %{ position: [900, 100], speed: [-10, 4] }
#      ]
		ships = Universe.MapStorage.get_all()
    push socket, "set_ships", ships
    {:noreply, socket}
  end

  def handle_in("refresh", _payload, socket) do
		ships = Universe.MapStorage.get_all()
    push socket, "set_ships", ships
    {:noreply, socket}
  end
  
  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (ships:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # This is invoked every time a notification is being broadcast
  # to the client. The default implementation is just to push it
  # downstream but one could filter or change the event.
  def handle_out(event, payload, socket) do
    push socket, event, payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
