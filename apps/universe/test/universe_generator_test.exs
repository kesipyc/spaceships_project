defmodule Universe.GeneratorTest do
  use ExUnit.Case

	test "generator generates" do
		Universe.Generator.start_link()
		assert Universe.Generator.next("sth") == 1
		assert Universe.Generator.next("sth") == 2

		assert Universe.Generator.next("other") == 1
		assert Universe.Generator.next("other") == 2
		
		assert Universe.Generator.next("sth") == 3
		assert Universe.Generator.next("other") == 3

		Universe.Generator.stop()
	end
end
