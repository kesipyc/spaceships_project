defmodule Universe.ShipTest do
  use ExUnit.Case

	test "vector of length 1 to the right" do
		{x, y} = Universe.Ship.vector(1, :math.pi / 2)
		assert_in_delta(x, 1, 0.00000001)
		assert_in_delta(y, 0, 0.00000001)
	end

	test "vector of length 1 to the top" do
		{x, y} = Universe.Ship.vector(1, 0.0)
		assert_in_delta(x, 0, 0.00000001)
		assert_in_delta(y, 1, 0.00000001)
	end

	test "vector of length 1 to the down" do
		{x, y} = Universe.Ship.vector(1, :math.pi)
		assert_in_delta(x, 0, 0.00000001)
		assert_in_delta(y, -1, 0.00000001)
	end

	test "vector of length 1 to the left" do
		{x, y} = Universe.Ship.vector(1, -:math.pi / 2)
		assert_in_delta(x, -1, 0.00000001)
		assert_in_delta(y, 0, 0.00000001)
	end

	test "vector of length 2 to the right" do
		{x, y} = Universe.Ship.vector(2, :math.pi / 2)
		assert_in_delta(x, 2, 0.00000001)
		assert_in_delta(y, 0, 0.00000001)
	end

	test "distance calculation" do
		assert Universe.Ship.distance({1, 1}, {4, 5}) == 5
	end
	
end
