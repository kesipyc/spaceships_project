defmodule Universe.Generator do
	#TODO: global registering for cluster
	use ExActor.GenServer, export: {:via, :gproc, {:n, :l, :universe_generator} }

	defstart start_link, do: initial_state(%{})

	defcall next(entity), state: state do
		{value, new_state} = Map.get_and_update(state, entity, fn current ->
			if current == nil do
				current = 0
			end

			{current + 1, current + 1}
		end)

		set_and_reply(new_state, value)
	end

	defcast stop, do: stop_server(:normal)
end
