defmodule Universe.CountersStorage do
	use GenServer

	def start_link do
		GenServer.start_link(__MODULE__, nil)
	end				
	
	def get_current_time(), do: get(:current_time)
	def inc_current_time(), do: inc(:current_time)
	def set_current_time(value) when is_integer(value), do: set(:current_time, value)

	def inc_ships_number(), do: inc(:ships_number)
	def dec_ships_number(), do: dec(:ships_number)
	def get_ships_number(), do: get(:ships_number)

	def inc_ships_moved_in_tick(tick)
	when is_integer(tick), do: inc({:ships_moved_in_tick, tick})
	def clear_ships_moved_in_tick(tick)
	when is_integer(tick), do: del({:ships_moved_in_tick, tick})

	
	
	def init(_) do
		:ets.new(:universe_counters, [:set, :named_table, :public, :compressed])
		#TODO: when persistant move this to other place so it's not reset 
	  set_current_time(1)
		{:ok, nil}
	end

	defp inc(name) do
		:ets.update_counter(:universe_counters, name, 1, {name, 0})
	end

	defp dec(name) do
		:ets.update_counter(:universe_counters, name, {2, -1, 0, 0}, {name, 0})
	end

	defp get(name) do
		:ets.lookup(:universe_counters, name)[name] || 0
	end

	defp set(name, value) when is_integer(value) do
		:ets.insert(:universe_counters, {name, value})
	end

	defp del(name) do
		:ets.delete(:universe_counters, name)
	end
end
