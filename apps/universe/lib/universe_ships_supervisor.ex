defmodule Universe.ShipsSupervisor do
	use Supervisor

	def start_link do
		Supervisor.start_link(__MODULE__, nil, name: :universe_ships_supervisor)
	end

	def add_ship(ship_id) do
		{status, _} = Supervisor.start_child(
			:universe_ships_supervisor,
			worker(Universe.Ship,
						 [{:ship, ship_id}],
						 restart: :transient, id: {:ship, ship_id}
			)
		)
	end
	
	def init(_) do
		processes = [
      worker(Universe.MapStorage, []),			
      worker(Universe.CountersStorage, []),
      worker(Universe.ShipsStorage, []),
			worker(Universe.Partition, [])			
		]
    opts = [strategy: :one_for_one, name: __MODULE__]
		
		supervise(processes, opts)
	end
end
