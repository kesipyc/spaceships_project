defmodule Universe.Ship do
	use GenServer

	require Logger
	
	@min_speed 0.05
	@max_speed 0.05
	@min_distance 1

	defstruct id: nil, position: {0, 0}, time: 0, speed: 0, angle: 0, next_position: {0, 0}, next_time: 0

	def start_link(id) do
		GenServer.start_link(__MODULE__, id)
	end

	def init(id) do
		GenServer.cast(self(), :startup)
		{:ok, id}
	end

	def handle_cast(:startup, state) do
		Universe.CountersStorage.inc_ships_number

		:ok = :pg2.create('ships')
		:ok = :pg2.join('ships', self())

		#initially ship may be later one tick
		#we call it as it's idempotent anyway
		#state may be id or ship (unlikely) at this point
		#but it will call right function anyway

		time = Universe.CountersStorage.get_current_time
		#call directly bypassing queue
		handle_cast({:tick, time}, state)
	end

	#after creation must retrieve state first (or generate)
	def handle_cast({:tick, time}=event, {:ship,_}=id) do
		ship = case Universe.ShipsStorage.read(id) do
						 [{^id, ship}] -> ship
						 _ -> create_random(id, {0, 0}, {1000, 1000}, time - 1)
					 end
		Universe.MapStorage.add_ship(ship.position)
		handle_cast(event, ship)
	end
	
	def handle_cast({:tick, tick_time}=event, %Universe.Ship{}=ship) do		
#		Logger.info("Ship #{inspect ship} event: #{inspect event}")
		if ship.next_time <= tick_time do
			ship_moving(event, ship)
		else
			time_moving(tick_time, ship)
		end
	end

	defp ship_moving({:tick, tick_time}=event, %Universe.Ship{}=ship) do
		#time to move
		new_ship = move(ship, {0, 0}, {1000, 1000})
			
		#ship was moved so time and next_time were updated
		if new_ship.time == tick_time do
			finish_tick(tick_time)
		end

		if new_ship.next_time <= tick_time do
			#just in case ship is late - this should never happen
			Logger.warn("Ship is moving more than once during one tick: #{inspect new_ship}")
		end
		handle_cast(event, new_ship)
	end

	defp time_moving(tick_time, %Universe.Ship{}=ship) do
		#not moving this tick
		if ship.time < tick_time do
			finish_tick(tick_time)
			{:noreply, %{ship | time: tick_time} }
		else
			{:noreply, ship}
		end
	end

	defp finish_tick(tick_time) do
		moved = Universe.CountersStorage.inc_ships_moved_in_tick(tick_time)
		if Universe.CountersStorage.get_ships_number == moved do
			Universe.Partition.tick_ready(tick_time)
		end
	end

	def handle_call(:destroy, _from, {:ship,_}=id) do
		Universe.ShipsStorage.delete(id)	
		Universe.CountersStorage.dec_ships_number
		{:stop, :normal, :ok, id}		
	end
	def handle_call(:destroy, from, ship) do
		Universe.MapStorage.remove_ship(ship.position)
		handle_call(:destroy, from, ship.id)
	end
	
	defp create_random(id, {positionX, positionY}, {sizeX, sizeY}, time) do
		x = :rand.uniform(sizeX) + positionX - 1
		y = :rand.uniform(sizeY) + positionY - 1
		speed = :rand.uniform() * (@max_speed - @min_speed) + @min_speed
		degrees = :rand.uniform() * 360
		radians = degrees * :math.pi / 180.0

		ship = %Universe.Ship
		{
			id: id,
			position: {x, y},
			time: time,
			speed: speed,
			angle: radians,
		}
		
		calculate_next(ship, {positionX, positionY}, {sizeX, sizeY})
	end

	def calculate_next(%Universe.Ship{} = ship,
										 {global_position_x, global_position_y},
										 {global_size_x, global_size_y}) do
		distance = @min_distance

    if ship.speed < @min_speed, do: ship = %{ship | speed:  @min_speed}

		{x, y} = ship.position
		{vector_x, vector_y} = vector(distance, ship.angle)
		next_position_x = x + vector_x
		next_position_y = y + vector_y

		time = ship.time
		next_time = time + round(distance / ship.speed)

		if (next_position_x < global_position_x) do
			next_position_x = next_position_x + global_size_x
		end
		if (next_position_x >= global_position_x + global_size_x) do
			next_position_x = next_position_x - global_size_x
		end
		if (next_position_y < global_position_y) do
			next_position_y = next_position_y + global_size_y
		end
		if (next_position_y >= global_position_y + global_size_y) do
			next_position_y = next_position_y - global_size_y
		end
		
		new_ship = %Universe.Ship{
			ship |
			next_position: {next_position_x, next_position_y},
			next_time: next_time
		}
		Universe.ShipsStorage.save(new_ship)
		new_ship
	end

	def move(%Universe.Ship{} = ship, global_position, global_size) do
		Universe.MapStorage.move_ship(ship.position, ship.next_position)
		calculate_next(
			%Universe.Ship{
				ship |
				position: ship.next_position,
				time: ship.next_time
			},
			global_position, global_size
		)
	end
	
	def distance({x1, y1}, {x2, y2}) do
		:math.sqrt(:math.pow(x2 - x1, 2) + :math.pow(y2 - y1, 2))
	end
									 
	def vector(length, angle) do
		{
			length * :math.sin(angle),
			length * :math.cos(angle)
		}
	end
end
