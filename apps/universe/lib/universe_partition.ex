defmodule Universe.Partition do
	require Logger
	#require HeapQueue
	
  use GenServer

  ## Client
	def start_link() do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

	def tick_ready(time) when is_integer(time) do
		GenServer.cast(__MODULE__, {:tick_ready, time})
	end

  def add_ships(n) when is_integer(n) do
    :erlang.send_after(1,__MODULE__, {:new_ship, n})
  end

  
	## Server

	def init(_state) do
		GenServer.cast(self(),:startup)
		{:ok, :erlang.timestamp}
	end

	def handle_call(:destroy, _from, state) do
		{:stop, :normal, :ok, state}		
	end

	def handle_cast(:startup, state) do
		Universe.new_ships(1)

		:ok = :pg2.create('ships')
#    fire_tick()
		{:noreply, state}
	end

	def handle_cast({:tick_ready, time}, state) do
		current_time = Universe.CountersStorage.get_current_time
		if time == 100 do
			state = :erlang.timestamp
		end
		if time == current_time do
			new_state = :erlang.timestamp
			if time > 100 do
				#Logger.info("Tick #{time}:  #{round(:timer.now_diff(new_state, state)/1000/(time-100))}ms")
			end
			fire_tick()
		else
			#This should never happen
			Logger.warn("{:tick_ready, #{time}} but current_time=#{current_time}")
		end
		{:noreply, state}			
	end

  def handle_info({:new_ship, 0}, state) do
		{:noreply, state}
  end
  def handle_info({:new_ship, n}, state) do
    Universe.ShipsSupervisor.add_ship(Universe.Generator.next("ship"))
    :erlang.send_after(1, self(), {:new_ship, n-1})
		{:noreply, state}
  end
  
	defp fire_tick() do
		time = Universe.CountersStorage.inc_current_time
#		Logger.info("Fire tick #{time}")
    Universe.MapStorage.save(time-1)
		Universe.CountersStorage.clear_ships_moved_in_tick(time-1)

#		GenServer.abcast([Node.self], :ship, {:tick, time})
		
		Enum.map(:pg2.get_members('ships'), fn pid ->
#			Logger.info("Sending to pid #{inspect pid} event #{inspect {:tick, time}}")
			GenServer.cast(pid, {:tick, time})
		end)
	end
	
	
	def transform(ships) do
		%{ships:
			 Enum.map(ships, fn(ship) ->
				 {x, y} = ship.position
				 %{ position: [round(x), round(y)] , speed: [0,0]}
			 end)
		 }
	end	
end
