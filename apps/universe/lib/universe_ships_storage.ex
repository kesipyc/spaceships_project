defmodule Universe.ShipsStorage do
	use GenServer

	def start_link do
		GenServer.start_link(__MODULE__, nil)
	end				
	
	def init(_) do
		:ets.new(:universe_ships, [:set, :named_table, :public, :compressed, {:write_concurrency, true}])
		{:ok, nil}
	end

	def read({:ship, id}) when is_integer(id) do
		:ets.lookup(:universe_ships, id)
	end
	
	def save(%Universe.Ship{id: {:ship, id}}=ship) when not(is_nil(id)) do
		true = :ets.insert(:universe_ships, {ship.id, ship})
	end

	def delete({:ship, id}) when is_integer(id) do
		true = :ets.delete(:universe_ships, id)
	end
end
