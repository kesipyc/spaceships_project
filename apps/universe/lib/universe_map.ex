defmodule Universe.MapStorage do
	use GenServer

	def start_link() do		
		GenServer.start_link(__MODULE__, nil, name: __MODULE__)
	end				

	def get_all do
		GenServer.call(__MODULE__, :get_all)
	end

	def save(time) do
		GenServer.call(__MODULE__, {:save, time})
	end
	
	def init(_) do
		:ets.new(:universe_map, [:set, :named_table, :public, :compressed])
		{:ok, nil}
	end

	def handle_call(:get_all, _from, state) do
		{:reply, state, state}
	end

	def handle_call({:save, time}, _from, _state) do
		save =
      %{
        ships: Enum.map(:ets.tab2list(:universe_map), fn {{x, y}, _} ->
          %{ position: [x, y] , speed: [0,0]}
        end)
      }
              
#    IO.inspect(save)
#		clear()
#		{:reply, :ok, {time, save}}
		{:reply, :ok, save}
	end
	
#	defp clear(), do: :ets.delete_all_objects(:universe_map)
	
	def add_ship({x, y}), do: inc({round(x),round(y)})
	def remove_ship({x, y}), do: dec({round(x),round(y)})


	def move_ship({from_x, from_y}, {to_x, to_y}) do
		remove_ship({from_x, from_y})
		add_ship({to_x, to_y})
	end
						 
	defp inc(name) do
		:ets.update_counter(:universe_map, name, 1, {name, 0})
	end

	defp dec(name) do
		if :ets.update_counter(:universe_map, name, {2, -1, 0, 0}, {name, 0}) == 0 do
      del(name)
    end
	end

	defp get(name) do
		:ets.lookup(:universe_map, name)[name] || 0
	end

	defp set(name, value) when is_integer(value) do
		:ets.insert(:universe_map, {name, value})
	end

	defp del(name) do
		:ets.delete(:universe_map, name)
	end
end
