defmodule Universe do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
		
    children = [
      # Define workers and child supervisors to be supervised
			worker(Universe.Generator, []),
			supervisor(Universe.ShipsSupervisor, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Universe.Supervisor]
    Supervisor.start_link(children, opts)
  end

	def new_ships(n) when is_integer(n) do
    Universe.Partition.add_ships(n)
	end
end
